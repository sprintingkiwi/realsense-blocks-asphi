﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net;
using System.Threading;
using System.Linq;
using System.Text;

public class StartServer : MonoBehaviour
{
    public WebServer ws;

    // Trackers
    public Dictionary<string, Vector3> trackers = new Dictionary<string, Vector3>();
    

    public void Start()
    {
        Debug.Log("ciao".Split(':'));

        ws = new WebServer(SendResponse, "http://localhost:8080/");
        ws.Run();
        Debug.Log("Started server on port 8080");

        // Populate trackers transforms list
        foreach (Transform t in GameObject.Find("Face Tracking").transform)
        {
            trackers.Add(t.name, t.position);
        }
    }

    public void Update()
    {
        RefreshTrackers();
    }    


    // Refresh trackers positions
    public void RefreshTrackers()
    {
        foreach (Transform t in GameObject.Find("Face Tracking").transform)
        {
            trackers[t.name] = t.position;
        }
    }

    // Get tracker position
    public string GetPosition(string trackerName)
    {
        string raw = trackers[trackerName].ToString();
        string final = raw.Replace(" ", "").Replace("(", "").Replace(")", "");
        return final;
    }

    // Respond to HTTP request
    public string SendResponse(HttpListenerRequest request)
    {
        string path = request.Url.LocalPath;
        string query = "";

        if (path.Contains(":"))
        {
            path = request.Url.LocalPath.Split(':')[0];
            query = request.Url.LocalPath.Split(':')[1];
        }
        
        Debug.Log("Path: " + path);
        Debug.Log("Query: " + query);

        switch (path)
        {
            case "/test":
                return "Hello World";

            case "/getpos":
                
                return GetPosition(query);

            default:
                return string.Format("<HTML><BODY>Welcome to ASPHI Snap extension webserver for SR300 Intel Realsense camera!<br>{0}</BODY></HTML>", DateTime.Now);
        }

    }
}
