﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shortcuts : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            Debug.Log("Quitting application");
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Running SNAP");
            //Debug.Log(System.IO.Path.GetFullPath("Assets/realsense-asphi-snap-project.xml"));
            System.Diagnostics.Process.Start("https://snap.berkeley.edu/snapsource/snap.html#open:https://bitbucket.org/sprintingkiwi/realsense-blocks-asphi/raw/master/Assets/realsense-asphi-snap-project.xml");
        }		
	}
}
